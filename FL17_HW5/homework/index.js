function isEqueals(x, y) {
  return x === y;
}

function isBigger(x, y) {
  return x > y;
}

function storeNames(...str) {
  return str;
}

function getDifference(x, y) {
  return isBigger(x, y) ? x - y : y - x;
}

function negativeCount(arr) {
  return arr.filter((el) => el < 0).length;
}

function letterCount(word, char) {
  return word.split('').filter((el) => el === char).length;
}

function countPoints(results) {
  const WINNER_POINT = 3;
  let count = 0;
  results.forEach((el) => {
    let [firstScore, secondScore] = el.split(':');
    if (+firstScore > +secondScore) {
      count += WINNER_POINT;
    } else if (+firstScore === +secondScore) {
      count += 1;
    }
  });
  return count;
}