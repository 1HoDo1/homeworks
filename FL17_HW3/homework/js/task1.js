let initialAmount, depositDuration, percentagePerYear;
const THOUSAND = 1000;
const HUNDRED = 100;
const NUMBER_TO_FIXED = 2;

while (initialAmount < THOUSAND || isNaN(initialAmount)) {
  if (initialAmount) {
    alert('Invalid input data');
  }
  initialAmount = parseInt(prompt('How much money do you want to deposit?'));
}

while (depositDuration < 1 || isNaN(depositDuration)) {
  if (depositDuration) {
    alert('Invalid input data');
  }
  depositDuration = parseInt(
    prompt('For how many years do you want to open a deposit?')
  );
}

while (
  percentagePerYear > HUNDRED ||
  percentagePerYear < 0 ||
  isNaN(percentagePerYear)
) {
  if (percentagePerYear) {
    alert('Invalid input data');
  }
  percentagePerYear = parseInt(prompt('What percentage of your deposit?'));
}

const getData = () => {
  const totalAmount = (
    initialAmount * Math.pow(1 + percentagePerYear / HUNDRED, depositDuration)
  ).toFixed(NUMBER_TO_FIXED);
  return [(totalAmount - initialAmount).toFixed(NUMBER_TO_FIXED), totalAmount];
};

function showResult(profit, amount) {
  alert(`
        Initial amount: ${initialAmount}
        Number of years: ${depositDuration}
        Percentage of year: ${percentagePerYear}
        Total profit: ${profit}
        Total amount: ${amount}
    `);
}

showResult(...getData());
