const DEFAULT_POCKETS_COUNT = 8;
const MULTIPLIER = 2;
const ATTEMPTS_COUNT = 3;
const INITIAL_PRIZE = 100;
const MAGNIFIER = 4;
let roundPrize = INITIAL_PRIZE;
let total = 0;
let pocketCount = DEFAULT_POCKETS_COUNT;

const showModal = (message) => {
  return confirm(message);
};

const random = (range) => {
  return Math.floor(Math.random() * (range + 1));
};

const checkRoundDecision = (decision, prize) => {
  if (decision) {
    return nextRound();
  }
  playAgainProposal(prize);
};

const playAgainProposal = (prize) => {
  alert(`Thank you for your participation. Your prize is: ${prize}$`);
  const userDecision = showModal('Do you want to play again?');
  if (userDecision) {
    return restart();
  }
};

const multiplyRoundPrize = () => {
  roundPrize *= MULTIPLIER;
}

const changeUserTotal = (prize, prizeDivider) => {
  total += prize / prizeDivider;
}

const userGuessing = (attempts, keyValue) => {
  let prizeDivider = 1;
  for (let attempt = 1; attempt <= attempts; attempt++) {
    const userChoice = parseInt(prompt('Please, input your number of pocket:'));
    if (userChoice === keyValue) {
      changeUserTotal(roundPrize, prizeDivider);
      const decision = showModal(
        `Congratulation, you won! Your prize is: ${total}$. Do you want to continue?`
      );
      magnifyPocketCount();
      multiplyRoundPrize();
      checkRoundDecision(decision, total);
      break;
    }
    prizeDivider *= MULTIPLIER;
  }
  playAgainProposal(total);
};

const startGame = () => {
  nextRound();
};

const nextRound = () => {
  const computerChoice = random(pocketCount);
  userGuessing(ATTEMPTS_COUNT, computerChoice);
};

const magnifyPocketCount = () => {
  pocketCount += MAGNIFIER;
}

const checkUserDecision = (decision) => {
  return decision
    ? startGame()
    : alert('You did not become a billionaire, but can.');
};

const clear = () => {
  roundPrize = INITIAL_PRIZE;
  total = 0;
  pocketCount = DEFAULT_POCKETS_COUNT;
};

const restart = () => {
  clear();
  startGame();
};

const init = () => {
  const decision = showModal('Do you want to play a game?');
  checkUserDecision(decision);
};

init();
