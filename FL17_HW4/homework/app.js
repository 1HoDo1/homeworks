const NEGATIVE_ONE = -1;
const MIN_AGE = 18;
const SLICER = -2;
function reverseNumber(num) {
  let result = '';
  let str = num.toString();
  const isNegative = str.includes('-');
  str = str.replace('-', '');
  for (let characterIdx = str.length - 1; characterIdx >= 0; characterIdx--) {
    result += str[characterIdx];
  }
  return isNegative ? parseInt(result) * NEGATIVE_ONE : parseInt(result);
}
function forEach(arr, func) {
  const newArr = [];
  for (let idx = 0; idx < arr.length; idx++) {
    newArr.push(func(arr[idx]));
  }
  return newArr;
}

function map(arr, func) {
  const newArr = [];
  for (let idx = 0; idx < arr.length; idx++) {
    newArr.push(arr[idx]);
  }
  return forEach(newArr, func);
}

function filter(arr, func) {
  const newArr = [];
  const filteredIds = forEach(arr, func);
  for (let idx = 0; idx < arr.length; idx++) {
    if (filteredIds[idx]) {
      newArr.push(arr[idx]);
    }
  }
  return newArr;
}

function getAdultAppleLovers(data) {
  return map(
    filter(data, (el) => el.age > MIN_AGE && el.favoriteFruit === 'apple'),
    (el) => el.name
  );
}

function getKeys(obj) {
  const newArr = [];
  for (let key in obj) {
    if (key) {
      newArr.push(key);
    }
  }
  return newArr;
}

function getValues(obj) {
  const newArr = [];
  for (let key in obj) {
    if (key) {
      newArr.push(obj[key]);
    }
  }
  return newArr;
}

function showFormattedDate(dateObj) {
  const MONTHS = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const month = MONTHS[dateObj.getMonth()];
  const year = dateObj.getFullYear();
  const day = ('0' + dateObj.getDate()).slice(SLICER);
  return `It is ${day} of ${month}, ${year}`;
}
